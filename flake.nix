{
  description = "samuelsung's build of dmenu";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "dmenu";
        pkgs = nixpkgs.legacyPackages.${system};
        package = pkgs.dmenu.overrideAttrs (_: {
          version = "master";
          src = ./.;
          patches = [ ];
        });
      in
      rec {
        apps = { ${name} = { type = "app"; program = "${defaultPackage}/bin/${name}"; }; };
        defaultPackage = package;
        packages.${name} = defaultPackage;
        defaultApp = apps.${name};
      }
    );
}
