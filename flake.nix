{
  description = "samuelsung's build of dmenu";

  inputs = {
    commitlint-config.url = "git+https://codeberg.org/samuelsung/commitlint-config";
    devenv.url = "github:cachix/devenv";
    flake-parts.url = "github:hercules-ci/flake-parts";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nix2container.url = "github:nlewo/nix2container";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        imports = [
          flake-parts.flakeModules.easyOverlay
          inputs.devenv.flakeModule
        ];
        systems = [ "x86_64-linux" ];
        perSystem = { config, system, pkgs, ... }:
          {
            apps = {
              "dmenu_run" = {
                type = "app";
                program = "${config.packages.dmenu-samuelsung}/bin/dmenu_run";
              };
            };

            overlayAttrs = {
              inherit (config.packages)
                dmenu-samuelsung;
            };

            packages.dmenu-samuelsung = pkgs.dmenu.overrideAttrs (_: {
              version = "master";
              src = ./.;
              patches = [ ];
            });

            checks = {
              inherit (config.packages)
                dmenu-samuelsung;
            };

            devenv.shells.default = {
              pre-commit.hooks = {
                deadnix.enable = true;
                nixpkgs-fmt.enable = true;
                statix.enable = true;

                commitlint = inputs.commitlint-config.hook.${system};
              };
            };
          };
      };
}
