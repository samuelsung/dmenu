/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 1;                    /* -c  option; if 1, dmenu appears at center     */
static int min_centered_width = 500;        /* minimum width when centered */
static int fuzzy = 1;                       /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "monospace:size=10";
static const char *fonts[] = {
	font,
	"monospace:size=10"
};
static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

static char normfgcolor[] = "#bbbbbb";
static char normbgcolor[] = "#222222";
static char selfgcolor[]  = "#eeeeee";
static char selbgcolor[]  = "#005577";
static char promptfgcolor[] = "#eeeeee";
static char promptbgcolor[] = "#005577";
static char selhighlightfgcolor[] =  "#ffc978";
static char selhighlightbgcolor[] = "#005577";
static char normhighlightfgcolor[] = "#ffc978";
static char normhighlightbgcolor[] = "#222222";
static char outfgcolor[] = "#000000";
static char outbgcolor[] = "#00ffff";
static char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { normfgcolor, normbgcolor },
	[SchemeSel] = { selfgcolor, selbgcolor },
	[SchemePrompt] = { promptfgcolor, promptbgcolor },
	[SchemeSelHighlight] = { selhighlightfgcolor, selhighlightbgcolor },
	[SchemeNormHighlight] = { normhighlightfgcolor, normhighlightbgcolor },
	[SchemeOut] = { outfgcolor, outbgcolor },
};

static VimKeys vimKeys = {
	.up    = XK_k,
	.down  = XK_j,
	.left  = XK_h,
	.right = XK_l,
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
static unsigned int min_centered_lines = 10;

/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/* -gx -gy options; */
static unsigned int x_gaps = 6;
static unsigned int y_gaps = 6;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
    { "normfgcolor",          STRING, &normfgcolor },
    { "normbgcolor",          STRING, &normbgcolor },
    { "selfgcolor",           STRING, &selfgcolor },
    { "selbgcolor",           STRING, &selbgcolor },
    { "promptfgcolor",        STRING, &promptfgcolor },
    { "promptbgcolor",        STRING, &promptbgcolor },
    { "selhighlightfgcolor",  STRING, &selhighlightfgcolor },
    { "selhighlightbgcolor",  STRING, &selhighlightbgcolor },
    { "normhighlightfgcolor", STRING, &normhighlightfgcolor },
    { "normhighlightbgcolor", STRING, &normhighlightbgcolor },
    { "outfgcolor",           STRING, &outfgcolor },
    { "outbgcolor",           STRING, &outbgcolor },
};
