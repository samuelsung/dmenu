/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
  "FiraCode Nerd Font Mono:size=10.5:antialias=true:autohint=true:style=Medium",
  "Noto Sans Mono CJK JP:size=10:style=Regular",
  "Noto Color Emoji:size=10:style=Regular"
};
static const char *prompt      = "dmenu";      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*                            fg         bg       */
	[SchemeNorm]          = { "#d8dee9", "#2e3440" },
	[SchemeSel]           = { "#2e3440", "#81a1c1" },
	[SchemeSelHighlight]  = { "#5e81ac", "#81a1c1" },
	[SchemeNormHighlight] = { "#ebcb8b", "#2e3440" },
	[SchemeMidHighlight]  = { "#ebcb8b", "#a3be8c" },
	[SchemeOut]           = { "#000000", "#00ffff" },
	[SchemeMid]           = { "#2e3440", "#a3be8c" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
static unsigned int lineheight = 0;         /* -h option; minimum height of a menu line     */

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
