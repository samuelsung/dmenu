/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 1;                    /* -c  option; if 1, dmenu appears at center     */
static int min_centered_width = 500;        /* minimum width when centered */
static int fuzzy = 1;                       /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "FiraCode Nerd Font Mono:size=10.5:antialias=true:autohint=true:style=Medium";
static const char *fonts[] = {
	font,
	"FiraCode Nerd Font Mono:size=10.5:antialias=true:autohint=true:style=Medium",
	"Noto Sans Mono CJK JP:size=10:style=Regular",
	"Noto Color Emoji:size=10:style=Regular"
};
static const char *prompt      = "dmenu";      /* -p  option; prompt to the left of input field */

static char normfgcolor[] = "#d8dee9";
static char normbgcolor[] = "#2e3440";
static char selfgcolor[]  = "#2e3440";
static char selbgcolor[]  = "#81a1c1";
static char promptfgcolor[] = "#2e3440";
static char promptbgcolor[] = "#a3be8c";
static char selhighlightfgcolor[] =  "#5e81ac";
static char selhighlightbgcolor[] = "#81a1c1";
static char normhighlightfgcolor[] = "#ebcb8b";
static char normhighlightbgcolor[] = "#2e3440";
static char outfgcolor[] = "#000000";
static char outbgcolor[] = "#00ffff";
static char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { normfgcolor, normbgcolor },
	[SchemeSel] = { selfgcolor, selbgcolor },
	[SchemePrompt] = { promptfgcolor, promptbgcolor },
	[SchemeSelHighlight] = { selhighlightfgcolor, selhighlightbgcolor },
	[SchemeNormHighlight] = { normhighlightfgcolor, normhighlightbgcolor },
	[SchemeOut] = { outfgcolor, outbgcolor },
};

static VimKeys vimKeys = {
	.up    = XK_e,
	.down  = XK_n,
	.left  = XK_h,
	.right = XK_i,
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
static unsigned int min_centered_lines = 20;

/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/* -gx -gy options; */
static unsigned int x_gaps = 6;
static unsigned int y_gaps = 6;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
    { "normfgcolor",          STRING, &normfgcolor },
    { "normbgcolor",          STRING, &normbgcolor },
    { "selfgcolor",           STRING, &selfgcolor },
    { "selbgcolor",           STRING, &selbgcolor },
    { "promptfgcolor",        STRING, &promptfgcolor },
    { "promptbgcolor",        STRING, &promptbgcolor },
    { "selhighlightfgcolor",  STRING, &selhighlightfgcolor },
    { "selhighlightbgcolor",  STRING, &selhighlightbgcolor },
    { "normhighlightfgcolor", STRING, &normhighlightfgcolor },
    { "normhighlightbgcolor", STRING, &normhighlightbgcolor },
    { "outfgcolor",           STRING, &outfgcolor },
    { "outbgcolor",           STRING, &outbgcolor },
};
